let express = require('express');
let app = express();
let server = require('http').Server(app);
var io = require('socket.io').listen(server);
var ForecastIo = require('forecastio');
var forecastIo = new ForecastIo('1521e57e29d9ef96eb0d2d6cedb43970');

var schedule = require('node-schedule');

let bodyParser = require('body-parser');

let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/testApp',{useMongoClient: true});

app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let SensorData = require('./models/sensordata');

var request = require('request');

var headers = {
  'User-Agent':       'Super Agent/0.0.1',
  'Content-Type':     'application/x-www-form-urlencoded'
}


function sendRequest(url,data) {
    var options = {
      url: url,
      method: 'POST',
      headers: headers,
      form: data
    }
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body);
      }
    });
  }


//Whenever someone connects this gets executed
io.on('connection', function(socket) {
 console.log('A user connected');
 SensorData.find({name:"light"},{_id:0,save_at:1,value:1}).sort({save_at:-1}).limit(20).exec(function (err,data){
  socket.emit("light",data[0]);
  socket.emit("graph-light",data);
  console.log(data);
});
 SensorData.find({name:"temperature"},{_id:0,save_at:1,value:1}).sort({save_at:-1}).limit(20).exec(function (err,data){
  socket.emit("temperature",data[0]);
  socket.emit("graph-temperature",data);
  console.log(data);
});
 SensorData.find({name:"humidity"},{_id:0,save_at:1,value:1}).sort({save_at:-1}).limit(20).exec(function (err,data){
  socket.emit("humidity",data[0]);
  socket.emit("graph-humidity",data);
  console.log(data);
});
 SensorData.find({name:"water_temperature"},{_id:0,save_at:1,value:1}).sort({save_at:-1}).limit(20).exec(function (err,data){
  socket.emit("water_temperature",data[0]);
  socket.emit("graph-water_temperature",data);
  console.log(data);
});
  SensorData.find({name:"ec"},{_id:0,save_at:1,value:1}).sort({save_at:-1}).limit(20).exec(function (err,data){
  socket.emit("ec",data[0]);
  socket.emit("graph-ec",data);
  console.log(data);
});

 var options = {
  units: 'si',
  exclude: 'currently,hourly,flags'
};
// forecastIo.forecast('13.7455095', '100.5353089').then(function(data) {
//   console.log(JSON.stringify(data, null, 2));
// });

   //Whenever someone disconnects this piece of code executed
   socket.on('disconnect', function () {
    console.log('A user disconnected');
  });
 });


app.get('/', function (req, res) {
  res.render("monitor",{});
});

var i = schedule.scheduleJob('* 6 * * *', function(){
    sendRequest("http://localhost:4000/send",{"source":232323,"destination":2164487438,"data":[255,0,111,33,221,112,232,111,67,66,88,41,76,60,44,93]});
  
});

var j = schedule.scheduleJob('* 20 * * *', function(){
    sendRequest("http://localhost:4000/send",{"source":232323,"destination":2164487438,"data":[0,0,111,33,221,112,232,111,67,66,88,41,76,60,44,93]});
  
});



app.get('/test', function (req, res) {
  sendRequest("http://localhost:4000/send",{"source":232323,"destination":2164487438,"data":[0,1,111,33,221,112,232,111,67,66,88,41,76,60,44,93]});
  res.send();
});
app.get('/test2', function (req, res) {
  sendRequest("http://localhost:4000/send",{"source":232323,"destination":2164487438,"data":[255,1,111,33,221,112,232,111,67,66,88,41,76,60,44,93]});
  res.send();
});

app.post('/', function (req, res) {
	var data = req.body;
	buf = Buffer.from(JSON.parse(data.data));
	// var buf = new Buffer(data.data,"ascii");
	// console.log(buf);
	// console.log(buf.readUInt16LE(0)); 
	// console.log(buf.readFloatLE(4)); 
	// console.log(buf.readFloatLE(8)); light

	var newData = SensorData({device_id:data.source,name:"temperature",value:buf.readFloatLE(8).toFixed(2)});
  newData.save(function(err) {
   if (err) throw err;
 });

  var newData = SensorData({device_id:data.source,name:"humidity",value:buf.readFloatLE(4).toFixed(2)});
  newData.save(function(err) {
   if (err) throw err;
 });

  var newData = SensorData({device_id:data.source,name:"light",value:buf.readUInt16LE(0)});
  newData.save(function(err) {
   if (err) throw err;
 });

  var newData = SensorData({device_id:data.source,name:"water_temperature",value:buf.readFloatLE(12).toFixed(2)});
  newData.save(function(err) {
   if (err) throw err;
 });

    var newData = SensorData({device_id:data.source,name:"ec",value:buf.readFloatLE(16).toFixed(2)});
  newData.save(function(err) {
   if (err) throw err;
 });

  io.sockets.emit("temperature",{value:buf.readFloatLE(8).toFixed(2)});
  io.sockets.emit("water_temperature",{value:buf.readFloatLE(12).toFixed(2)});
  io.sockets.emit("humidity",{value:buf.readFloatLE(4).toFixed(2)});
  io.sockets.emit("light",{value:buf.readUInt16LE(0)});
  io.sockets.emit("ec",{value:buf.readFloatLE(16).toFixed(2)});

  res.send('');
});

server.listen(5000, function () {
  console.log('Example app listening on port 5000!'); 
});